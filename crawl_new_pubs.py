import sqlite3
import requests
import json
import datetime
import logging

# Date to start '2017-03-24T10:55:23.926019489'

i = datetime.datetime.now()

con = sqlite3.connect('date_and_pub_uri.db')
c = con.cursor()

c.execute("DROP TABLE if exists InriaUri")
con.commit()

c.execute("CREATE TABLE InriaUri (uri text)")
con.commit()

c.execute("SELECT * FROM Date_update")

for date in c :
    date = date[0]
logging.info("Date Start : "+date)

c.execute("DROP TABLE if exists Date_update")
con.commit()

c.execute("CREATE TABLE Date_update (date text)")
con.commit()

c.execute("""INSERT INTO Date_update(date) VALUES (?);""", (i.isoformat(),))
con.commit()

r = requests.get('https://api.archives-ouvertes.fr/search/inria/?q=structure_t:inria&fq=submittedDate_tdate:['+date+'Z%20TO%20*]')
convert_json = json.loads(r.text)
numFound = convert_json["response"]["numFound"]
logging.info("Nb publications : "+ str(numFound))

c.execute("DROP TABLE if exists NbFound")
con.commit()

c.execute("CREATE TABLE NbFound (nbfound int)")
con.commit()

c.execute("""INSERT INTO NbFound(nbfound) VALUES (?);""", (numFound,))
con.commit()

nb_range = int(numFound/1000) + 1

for i in range(0, nb_range) :
    r = requests.get('https://api.archives-ouvertes.fr/search/inria/?q=structure_t:inria&fq=submittedDate_tdate:['+date+'Z%20TO%20*]&start='+str(i*1000)+'&rows=1000')
    convert_json = json.loads(r.text)
    for docu in  convert_json["response"]["docs"] :
        c.execute("""INSERT INTO InriaUri(uri) VALUES (?);""", (docu['uri_s'],))
    con.commit()
