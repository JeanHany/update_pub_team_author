from elasticsearch import Elasticsearch
import configparser
import datetime
import trigrams_with_significant_keywords

def create_teams(pub, team_done) :
    for aff in pub["_source"]["affiliations"] :
        if aff["type"] == "researchteam" :
            for relation in aff["relations"]["indirect"] :
                if relation["acronym"] == "INRIA" and aff["acronym"] not in team_done :
                    query = {"query" : {"match" : {"acronym" : aff["acronym"]}}}
                    result = es.search(index=index_team, doc_type=doc_type_team, body=query)["hits"]["hits"]
                    if len(result) > 0 :
                        update_teams(aff["acronym"], result)
                    else :
                        create_new_teams(aff["acronym"])
                    team_done.append(aff["acronym"])
    return team_done

def create_new_teams(acronym) :
    pubs = get_pub_acronym(acronym)
    all_pubs = []
    i = 0
    completeText = ""
    new_info = {}
    for pub in pubs :
        if i == 0 :
            for aff in pub["_source"]["affiliations"]:
                if aff["acronym"] == acronym :
                    new_info["id"] = aff["id"]
                    new_info["type"] = aff["type"]
                    new_info["address"] = aff["address"]
                    new_info["status"] = aff["status"]
                    new_info["country_key"] = aff["country_key"]
                    new_info["name"] = aff["name"]
                    new_info["url"] = aff["url"]
                    if aff["date_end"] != "" :
                        new_info["date_end"] = aff["date_end"]
                    else :
                        new_info["date_end"] = "unknow"
                    if aff["date_start"] != "" :
                        new_info["date_start"] = aff["date_start"].split("-")[0]
                    else :
                        new_info["date_start"] = str(datetime.datetime.now().year)
                    new_info["acronym"] = aff["acronym"]
        i+=1
        title_fr = pub["_source"]["title_fr"]
        title_en = pub["_source"]["title_en"]
        abstract_fr = ""
        abstract_en = ""
        if "abstract_fr" in pub["_source"] :
            abstract_fr = pub["_source"]["abstract_fr"]
        if "abstract_en" in pub["_source"] :
            abstract_en = pub["_source"]["abstract_en"]
        completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
        completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
        del pub["_source"]["affiliations"]
        del pub["_source"]["projects"]
        all_pubs.append(pub["_source"])
    new_info["pubs"] = all_pubs
    # keywords = get_keywordsTeam(acronym)
    data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, [])
    new_info["main_topics"] = data_list
    es.index(index=index_team, doc_type=doc_type_team, id=new_info["id"], body=new_info)

def update_teams(acronym, result) :
    pubs = get_pub_acronym(acronym)
    new_pubs = {}
    new_pubs["pubs"] = []
    if "pubs" in result[0]["_source"] :
        new_pubs["pubs"] = result[0]["_source"]["pubs"]
    completeText = ""
    for pub in pubs :
        del pub["_source"]["affiliations"]
        del pub["_source"]["projects"]
        new_pubs["pubs"].append(pub["_source"])
        title_fr = pub["_source"]["title_fr"]
        title_en = pub["_source"]["title_en"]
        abstract_fr = ""
        abstract_en = ""
        if "abstract_fr" in pub["_source"] :
            abstract_fr = pub["_source"]["abstract_fr"]
        if "abstract_en" in pub["_source"] :
            abstract_en = pub["_source"]["abstract_en"]
        completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
        completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
    keywords = get_keywordsTeam(acronym)
    data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
    new_pubs["main_topics"] = []
    if "main_topics" in result[0]["_source"] :
        new_pubs["main_topics"] = result[0]["_source"]["main_topics"]
    new_pubs["main_topics"] = data_list + new_pubs["main_topics"]
    es.update(index=index_team, doc_type=doc_type_team, id=result[0]["_id"] ,body={ "doc" : new_pubs})

def get_pub_acronym(acronym) :
    query = {
    "query": {
        "nested": {
           "path": "affiliations",
           "query": {
               "match": {
                  "affiliations.acronym": acronym
               }
           }
        }
    }
    }
    return es.search(index=index_pub_temp, doc_type=doc_type_pub_temp, body=query)["hits"]["hits"]

def get_keywordsTeam(acronym) :
    query_keywords = {
                    "query": {
                        "match_all": {}
                    },
                    "aggregations": {
                        "affiliations": {
                            "nested": {
                                "path": "affiliations"
                            },
                            "aggs": {
                                "affiliationfilter": {
                                    "filter": {
                                        "term": {
                                            "affiliations.acronym": acronym
                                        }
                                    },
                                    "aggs": {
                                        "got_back": {
                                            "reverse_nested": {},
                                            "aggs": {
                                                "most_sig_words": {
                                                    "significant_terms": {
                                                        "field": "title_abstract_sign",
                                                        "size": 100
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
    }
    result = es.search(index=index_pub, doc_type=doc_type_pub, body=query_keywords)
    result = result["aggregations"]["affiliations"]["affiliationfilter"]["got_back"]["most_sig_words"]["buckets"]
    return result

def create_authors(pub, author_done) :
    for author in pub["_source"]["authors"] :
        if author["formatName"] not in author_done :
            query = {"query" : {"match" : {"formatName" : author["formatName"]}}}
            result = es.search(index=index_author, doc_type=doc_type_author, body=query)["hits"]["hits"]
            if len(result) > 0 :
                result_team = ""
                for aff in pub["_source"]["affiliations"] :
                    if author["affiliation"].replace("#", "") == aff["id"]:
                        query = {"query" : {"match" : {"acronym" : aff["acronym"]}}}
                        result_team = es.search(index=index_team, doc_type=doc_type_team, body=query)["hits"]["hits"]
                        break
                if len(result_team) > 0 :
                    update_authors(author["formatName"], result, result_team)
            else :
                result = ""
                for aff in pub["_source"]["affiliations"] :
                    if author["affiliation"].replace("#", "") == aff["id"] :
                        query = {"query" : {"match" : {"acronym" : aff["acronym"]}}}
                        result = es.search(index=index_team, doc_type=doc_type_team, body=query)["hits"]["hits"]
                        break
                if result != "" and len(result) > 0 :
                    create_new_authors(author["formatName"], author, result)
            author_done.append(author["formatName"])
    return author_done

def create_new_authors(formatName, author, result) :
    new_author = {}
    new_author["surname"] = author["surname"]
    new_author["forename"] = author["forename"]
    new_author["first_appear"] = datetime.datetime.now().year
    new_author["last_appear"] = "unknow"
    new_author["category"] = "unknow"
    new_author["structure"] = result[0]["_id"]
    new_author["acronym"] = result[0]["_source"]["acronym"]
    new_author["moreinfos"] = "unknow"
    new_author["formatName"] = formatName
    pubs = get_pub_formatName(formatName)
    list_pub = []
    new_author["pubs"] = []
    completeText = ""
    for pub in pubs :
        del pub["_source"]["affiliations"]
        del pub["_source"]["projects"]
        new_author["pubs"].append(pub["_source"])
        list_pub.append(pub["_source"]["halId"])
        title_fr = pub["_source"]["title_fr"]
        title_en = pub["_source"]["title_en"]
        abstract_fr = ""
        abstract_en = ""
        if "abstract_fr" in pub["_source"] :
            abstract_fr = pub["_source"]["abstract_fr"]
        if "abstract_en" in pub["_source"] :
            abstract_en = pub["_source"]["abstract_en"]
        completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
        completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
    # keywords = get_keywordsResearcher(list_pub)
    new_author["main_topics"] = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, [])
    es.index(index=index_author, doc_type=doc_type_author, body=new_author)

def update_authors(formatName, result, result_team) :
    pubs = get_pub_formatName(formatName)
    new_pubs = {}
    new_pubs["pubs"] = []
    new_pubs["pubs"] = result[0]["_source"]["pubs"]
    test2 = result[0]["_source"]["acronym"]
    test = result_team[0]["_source"]["acronym"]
    if test != test2 :
        new_pubs["structure"] = result_team[0]["_id"]
        new_pubs["acronym"] = result_team[0]["_source"]["acronym"]
        history = {}
        history ["category"] = "unknow"
        history ["structure"] = result_team[0]["_id"]
        history ["acronym"] = result_team[0]["_source"]["acronym"]
        history ["moreinfos"] = "unknow"
        history ["first_appear"] = datetime.datetime.now().year
        history ["last_appear"] = "unknow"
        result[0]["_source"]["history"].append(history)
        new_pubs["history"] = result[0]["_source"]["history"]
    list_pub = []
    completeText = ""
    for pub in pubs :
        del pub["_source"]["affiliations"]
        del pub["_source"]["projects"]
        new_pubs["pubs"].append(pub["_source"])
        list_pub.append(pub["_source"]["halId"])
        title_fr = pub["_source"]["title_fr"]
        title_en = pub["_source"]["title_en"]
        abstract_fr = ""
        abstract_en = ""
        if "abstract_fr" in pub["_source"] :
            abstract_fr = pub["_source"]["abstract_fr"]
        if "abstract_en" in pub["_source"] :
            abstract_en = pub["_source"]["abstract_en"]
        completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
        completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : ""} )
    keywords = get_keywordsResearcher(list_pub)
    main_topics = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
    new_pubs["main_topics"] = main_topics + result[0]["_source"]["main_topics"]
    es.update(index=index_author, doc_type=doc_type_author, id=result[0]["_id"] ,body={ "doc" : new_pubs})

def get_pub_formatName(formatName) :
    query = {
    "query": {
        "nested": {
           "path": "authors",
           "query": {
               "match": {
                  "authors.formatName": formatName
               }
           }
        }
    }
    }
    return es.search(index=index_pub_temp, doc_type=doc_type_pub_temp, body=query)["hits"]["hits"]


def get_keywordsResearcher(halId) :
    query_keywords = {
                "aggregations": {
                    "all_pub": {
                        "global": {},
                        "aggs": {
                            "pub_filter": {
                                "filter": {
                                    "terms": {
                                        "halId": halId
                                    }
                                },
                                "aggs": {
                                    "most_sig_words": {
                                        "significant_terms": {
                                            "field": "title_abstract_sign",
                                            "size": 100
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
    }
    result = es.search(index=index_pub, doc_type=doc_type_pub, body=query_keywords)
    if "most_sig_words" in result["aggregations"]["all_pub"]["pub_filter"] :
        result = result["aggregations"]["all_pub"]["pub_filter"]["most_sig_words"]["buckets"]
    else :
        result = []
    return result

def get_all_pubs():
    query = {"size" : 1000, "query" : {"match_all" : {}}}
    return es.search(index=index_pub_temp, doc_type=doc_type_pub_temp, body=query, scroll="20m")

def nb_pubs() :
    query = {"query" : {"match_all" : {}}}
    return es.search(index=index_pub_temp, doc_type=doc_type_pub_temp, body=query)["hits"]["total"]

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_pub_temp = config.get("elasticsearch", "index_temp_pub")
doc_type_pub_temp = config.get("elasticsearch", "doc_type_temp_pub")
index_pub = config.get("elasticsearch", "index_pub")
doc_type_pub = config.get("elasticsearch", "doc_type_pub")
index_team = config.get("elasticsearch", "index_team")
doc_type_team = config.get("elasticsearch", "doc_type_team")
index_author = config.get("elasticsearch", "index_author")
doc_type_author = config.get("elasticsearch", "doc_type_author")
index_team_temp = config.get("elasticsearch", "index_team_temp")
index_author_temp = config.get("elasticsearch", "index_author_temp")

nb_pubs = nb_pubs()
nb_range = int(nb_pubs/1000) + 1

team_done = []
author_done = []

for i in range(0, nb_range) :
    if i == 0 :
        pubs = get_all_pubs()
    else :
        pubs = es.scroll(scroll_id=scroll_id, scroll="20m")
    scroll_id = pubs["_scroll_id"]
    for pub in pubs["hits"]["hits"] :
        team_done = create_teams(pub, team_done)
        author_done = create_authors(pub, author_done)
    es.index(index=index_pub, doc_type=doc_type_pub, id=pub["_id"], body=pub["_source"])
    es.delete(index=index_pub_temp, doc_type=doc_type_pub_temp, id=pub["_id"])
