# Update data with HAl

Update pub team and author with only data from HAL

## Get new publication

Get new publications by submit date.
For all publications check if we need to update teams, authors or create new teams, authors

## Update teams or authors

### Update team

If team already in Elasticsearch, we can update the team with is new publications and new topics.

### Update author

If author already in Elasticsearch, we can update the author with is new publications and new topics.

## Check if new teams or authors

### New team

if team not in Elasticsearch, we create a new team with the metadata available, publications and new topics.

### New author

if author not in Elasticsearch, we create a new author with the metadata available, publications and new topics.


## How it works

* Get new publication by submit date
* Index new publications in temp index
* Create/update authors and teams
* index new publications

This works for INRIA and IRISA. If the structure of the document index in HAL are correct.
The structure should be :

* author -> researchteam

```xml
<author role="aut">
  <persName>
    <forename type="first">Remi</forename>
    <surname>Ramadour</surname></persName>
    <idno type="halauthorid">987494</idno>
    <affiliation ref="#struct-2520"/>
    <affiliation ref="#struct-241669"/>
</author>
```

The element affiliation with attribute ref correspond to a researchteam (struct-2520). Struct-250 correspond to Lagadic (look under).

* team (type=researchteam) -> relation (acronym=INRIA)


```xml
<org type="researchteam" xml:id="struct-2520" status="OLD">
  <idno type="RNSR">200418341Y</idno>
  <orgName>Visual servoing in robotics, computer vision, and augmented reality</orgName>
  <orgName type="acronym">Lagadic</orgName>
  <desc>
    <address><country key="FR"/></address>
    <ref type="url">http://www.inria.fr/equipes/lagadic</ref>
  </desc>
    <listRelation>
      <relation active="#struct-34586" type="direct"/>
      <relation active="#struct-300009" type="indirect"/>
      <relation active="#struct-419153" type="direct"/>
      <relation active="#struct-419366" type="direct"/>
      <relation active="#struct-105128" type="indirect"/>
      <relation active="#struct-172265" type="indirect"/>
      <relation name="UMR6074" active="#struct-441569" type="indirect"/>
      <relation active="#struct-247362" type="indirect"/>
      <relation name="- RENNES" active="#struct-301232" type="indirect"/>
      <relation active="#struct-301262" type="indirect"/>
      <relation active="#struct-105160" type="indirect"/>
      <relation active="#struct-411575" type="indirect"/>
    </listRelation>
</org>
```

```xml
<org type="institution" xml:id="struct-300009" status="VALID">
  <orgName>Institut National de Recherche en Informatique et en Automatique</orgName>
  <orgName type="acronym">Inria</orgName>
  <desc>
    <address><addrLine>Domaine de VoluceauRocquencourt - BP 10578153 Le Chesnay Cedex</addrLine>
    <country key="FR"/></address>
    <ref type="url">http://www.inria.fr/en/</ref>
  </desc>
</org>
```

The team Lagadic had a relation with the struct-300009 witch is INRIA.
